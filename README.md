## Ambiente

Esta receta fue desarrollada en el siguiente ambiente:

| | |
|-|-|
| Ambiente | Linux |
| Versión de docker-compose | 1.25.5 |
| Versión de docker | 19.03.8-ce |

## Credenciales predeterminadas

| | |
|-|-|
| Username | admin |
| Password | admin |

## Instalación

Para crear este ambiente es necesario incrementar el tamaño de memoria para máquinar virtuales; esto se hace con el siguiente comando

```bash
sysctl -w vm.max_map_count=262144
```

Para agilizar la ejecución se agrega el script de bash **_start_**.



## Uso

Una vez levantado el ambiente la interfaz web de sonar queda en [localhost:9000](http://localhost:9000).

Para iniciar un análisis con maven ejecutar

```bash
mvn sonar:sonar
```

> No se deben agregar los parámetros al archivo pom.xml ya que se está apuntando a una instancia local.


Si se puede usar [sonar-scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/), esto es particularmente útil cuando el projecto no es en Java.

Similarmente, existe un [plugin de gradle](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-gradle/) para sonar.



